import 'dart:io';

bool funcCheckLottery(String lotto) {
  // var data = <String>{'222', '0805'};
  // for (String d in data) {
  //   if (d.compareTo(lotto) == 0) {
  //     return true;
  //   }
  // }

  String firstPrice = "436594";
  String firstPrice0 = "436593";
  String firstPrice2 = "436595";
  var threeBefore = <String>{'893', '266'};
  var threeAfter = <String>{'447', '282'};
  String twoAfter = "14";
  var secondPrice = <String>{'502412', '285563', '396501', '084971', '049364'};
  var thirdPrice = <String>{
    '996939',
    '043691',
    '422058',
    '853019',
    '662884',
    '166270',
    '666926',
    '896753',
    '242496',
    '575619'
  };
  var forthPrice = <String>{
    '345541',
    '675634',
    '240354',
    '124121',
    '263978',
    '293437',
    '098790',
    '011052',
    '550340',
    '400877',
    '451697',
    '651710',
    '696914',
    '816411',
    '491669'
  };
  var fifthPrice = <String>{
    '033687',
    '787212',
    '169194',
    '505423',
    '328183',
    '847630',
    '743359',
    '871886',
    '236234',
    '292486',
    '029793',
    '219615',
    '878866',
    '974474',
    '028671'
  };

  if (firstPrice.compareTo(lotto) == 0) {
    print('You get the first price 6,000,000 baht');
    return true;
  }

  if (firstPrice0.compareTo(lotto) == 0) {
    print('You get near the first price 100,000 baht');
    return true;
  }

  if (firstPrice2.compareTo(lotto) == 0) {
    print('You get near the first price 100,000 baht');
    return true;
  }

  for (String p in threeBefore) {
    if (p.contains(lotto.substring(0, 3))) {
      print('You get three before price 4,000 baht');
      return true;
    }
  }

  for (String p in threeAfter) {
    if (p.contains(lotto.substring(3, 6))) {
      print('You get three after price 4,000 baht');
      return true;
    }
  }

  if (twoAfter.contains(lotto.substring(4, 6))) {
    print('You get two after price 2,000 baht');
    return true;
  }

  for (String p in secondPrice) {
    if (p.compareTo(lotto) == 0) {
      print('You get the second price 200,000 baht');
      return true;
    }
  }

  for (String p in thirdPrice) {
    if (p.compareTo(lotto) == 0) {
      print('You get the third price 80,000 baht');
      return true;
    }
  }

  for (String p in forthPrice) {
    if (p.compareTo(lotto) == 0) {
      print('You get the forth price 40,000 baht');
      return true;
    }
  }

  for (String p in fifthPrice) {
    if (p.compareTo(lotto) == 0) {
      print('You get the fifth price 20,000 baht');
      return true;
    }
  }

//https://www.thairath.co.th/lottery

  return false;
}

// bool checkPrice(String lotto, {String? price}) {
//   for (String p in price) {
//      if (p.compareTo(lotto) == 0) {
//       return true;
//      }
//   }
//   return false;
// }

void main() {
  print(" \n Please input your lottery ");
  String lotto = (stdin.readLineSync()!);
  if (funcCheckLottery(lotto)) {
    print("Congratulation! $lotto");
  } else {
    print("Oh! Sorry with you! $lotto");
  }
}
