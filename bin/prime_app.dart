import 'dart:io';

bool funcPrime(int p) {
  if (p == 1) return false;
  for (int i = 2; i < p; i++) {
    if (p % i == 0) {
      return false;
    }
  }
  return true;
}

void main() {
  print(" \n please input your number ");
  int? num = int.parse(stdin.readLineSync()!);
  if (funcPrime(num)) {
    print("$num is Prime Number");
  } else {
    print("$num isn't Prime Number");
  }
}
