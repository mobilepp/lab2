import 'dart:io';

void func3(int a, {int b: 12}) {
  print(" Value of a is $a ");
  print(" Value of b is $b ");
}

void main() {
  // Calling function with default valued parameter
  print(" \n Calling function with default valued parameters ");
  func3(01);
}


/*void func1(var a, {var b, var c}) {
  print(" Value of a is $a ");
  print(" Value of b is $b ");
  print(" Value of b is $c ");
}

void main() {
  // Calling the function with optional parameter
  print(" Calling the function with optional parameters : ");
  func1(01, c: 12);
}*/

/*
void func1(var a, [var b]) {
  print(" Value of a is $a ");
  print(" Value of b is $b ");
}

void main() {
  // Calling the function with optional parameter
  print(" Calling the function with optional parameters : ");
  func1(01);
}
 */


/*enum college_semester {
  first,
  second,
  third,
  fourth,
  fifth,
  sixth,
  seventh,
  eighth
}

void main() {
  print(college_semester.values);
  print(" \n ");
  college_semester.values
      .forEach((v) => print(' value : $v, index : ${v.index} '));
  print(" \n ");
  print(' You are in : ${college_semester.seventh} ');
  print(" \n ");
  print(
      ' College semester 3rd corresponds to index : ${college_semester.seventh.index} ');
}*/



/*enum prog_language {
  Dart,
  Flutter,
  Java,
  JavaScript,
  Python,
  PHP,
  C,
  Go,
  SQL,
  MongoDb
}

void main() {
  print(" Here is the list of Programming Language Enumeration ");
  print(prog_language.values);
  prog_language.values
      .forEach((i) => print(' Value : $i, Index : ${i.index} '));
  print(prog_language.values[5]);
}*/


// using map constructor
/*void main() {
  // declaring a map using a map( ) constructor
  var maps1 = new Map();

  // initializing keys with values
  maps1[1] = 'one';
  maps1[2] = 'two';
  maps1[3] = 'three';

  print(maps1);

  // using property keys. It prints all the keys of the given map
  print("\n The keys of the map are : ${maps1.keys} ");

  // using property values. It prints all the values of the given map
  print("\n The values of the map are : ${maps1.values} ");

  // using property length. It prints the number of key – value pairs in the map
  print("\n The length of the map is : ${maps1.length} \n ");

  /// using property isEmpty. It returns true or false depending upon the
  /// availability of key-value pair in the map
  print(maps1.isEmpty);
  print(" \n ");

  /// using property isNotEmpty. It returns true or false depending upon the
  /// availability of key - value pair in the map
  print(maps1.isNotEmpty);
}*/


// using map constructor
/*void main() {
  // declaring a map using a map( ) constructor
  var maps1 = new Map();
  // initializing keys with values
  maps1[1] = 'one';
  maps1[2] = 'two';
  maps1[3] = 'three';

  print(maps1);
}*/


// Set Operations in Dart
 
/*void main( )
{
  // declaring set 1 with string values
  var set1 = < String > { "India", "will", "win", "T-20" };
   
  // printing the values of set 1
  print( " Set 1 is as follows : " );
  print( set1 );
   
  // declaring set 2 with string values
  var set2 = < String > { "World", "Cup", "this", "year." };
  
  // declaring set 3 with string values
  var set3 = < String > { "World", "India", "win", "year." };
   
  // printing the values of set 2
  print( "\n\n Set 2 is as follows : " );
  print( set2 );

  // Performing union on set1 and set2
  print( "\n Union of two sets is ${ set1.union( set2 ) } \n " ) ;
   
  // Performing intersection on set1 and set2
  print( " Intersection of sets 1 and 2 is  ${ set1.intersection( set2 ) } \n ");
  
  // Performing intersection on set2 and set3
  print( " Intersection of sets 2 and 3 is ${ set2.intersection( set3 ) } \n " );
   
  // Performing difference ( Set2 - Set1 )
  print( " Difference of sets 2 and 1 is ${ set2.difference( set1 ) } \n " );
  
  // Performing difference ( Set1 - Set2 )
  print( " Difference of sets 1 and 2 is ${ set1.difference( set2 ) } \n " );
  
  // Performing difference ( Set2 - Set3 )
  print( " Difference of sets 2 and 3 is ${ set2.difference( set3 ) } \n " );

  // Performing union on set2 and set3
  print( " Union of two sets is ${ set2.union( set3 ) } \n " ) ;
}*/


/*void main() {
  // declares and defines set languages
  var languages = {'Dart', 'Flutter', 'Python', 'Java'};
  // prints the values of set
  print(languages);

  // creates an empty string
  var lang = <String>{};
  print(lang);
  // add( ) to add the value in set languages
  languages.add('php');
  print(languages);
  languages.add('php');
  print(languages);
  var listt = [1, 2, 3];
  listt.add(4);
  print(listt);
  listt.add(4);
  print(listt);
}*/


/*void main() {
  var list1 = [1, 2, 3];
  var list2 = [1];
  print(' \nList 1 : ');
  print(list1);
  print(' \nList 2 : ');
  print(list2);
  list1.add(4);
  print(' \nList 1 after adding 4 : ');
  print(list1);
  list1.addAll([5, 6, 7, 8]);
  print(' \nList 1 after adding 5, 6, 7, 8 : ');
  print(list1);
  list2.insert(1, 11);
  list2.insert(2, 12);
  print(' \nList 2 after adding 11 and 12 at 1st and 2nd index : ');
  print(list2);
  list2.insertAll(2, [13, 14, 15, 16]);
  print(' \nList 2 after adding 13, 14, 15, 16 at index 2 : ');
  print(list2);
  list2.replaceRange(2, 4, [21, 22, 23]);
  print(
      ' \nList 2 after replacing elements between 2nd and 4th index  with 21, 22, 23 :');
  print(list2);
  list2.remove(21);
  print(' \nList 2 after removing 21 value : ');
  print(list2);
  list1.removeAt(2);
  print(' \nList 1 after removing element at the 2nd index : ');
  print(list1);
  list2.removeLast();
  print(' \nList 2 after removing the last element : ');
  print(list2);
  list1.removeRange(1, 3);
  print(
      ' \nList 1 after adding removing the elements between 1st and 3rd index : ');
  print(list1);
}*/


/*void main( )
{
  // defining a list
  var list = [ 1, 2, 3 ];
  var list1;
  // prints the length of the list
  print(list.length);
  // prints the element at the 1st index of the list
  print(list [ 1 ]);
  //inserts value of the list in list2
  var list2 = [0,...list];
  // inserts value of list1 in list2. ? is used to check in case the list1 is null
  var list3 = [0,...?list1];
  print(list2);
  print(list3);
}
*/

/*void main( )
{
  // declaring Boolean variables
  bool check1, check2;
  check1 = 5 == 6;
  check2 = 7 > 4;

  // boolean has two values true and false
  // based on above operations, true or false will be printed
  print( check1 );
  print( check2 );
}*/


/*void main( )
{
  // no type annotation. Compiler automatically infers integer value
  var a = 5 ;
  // Compiler automatically infers double value
  var b = 5.5 ;
  // declaring c as integer type
  int c = 6 ;
  // declaring variables as double type
  double d = 6.2 ;
  double e = 5 ; //compiler implicitly converts integer into double value when required
  /* with num compiler automatically interprets integer or double type by
  analyzing the value assigned */
  num f = 9 ;
  num g = 9.9 ;
  print( ' \n Value of a : $a \n Value of b : $b \n Value of c : $c \n Value of d : $d \n Value of e : $e \n Value of f : $f \n Value of g : $g ' );
}*/


/*import 'dart:io';

void main() {
  // write( ) function
  stderr.write(' This illustrates ');
  stdout.write(' write( ) function. \n ');
  // writeln( ) function
  stdout.writeln(' This illustrates ');
  stdout.writeln(' writeln( ) function. \n ');
  // print( ) function
  print(' This illustrates ');
  print(' print( ) function. ');
}*/

/*import 'dart:io';
void main( )
 {
 print( 'Enter the value of a : ' );
 //int? a = int.parse(stdin.readLineSync( )! ); //accepts integer value
 double? a = double.parse(stdin.readLineSync( )! );
 print( 'Enter the value of b : ' );
 //int? b = int.parse( stdin.readLineSync( )! );//accepts integer value
 double? b = double.parse( stdin.readLineSync( )! ); 
//int mul = a * b; // stores product of a and b in mul
double mul = a * b; // stores product of a and b in mul
 print( 'Product of these two numbers is : $mul' );
}*/

/*import 'dart:io';
void main( )
{
  print( ' Enter your favourite coding language : ' );
  // inputs string from the user
  String? code_lang = stdin.readLineSync( );
  // Printing the string
  print( '\nGreat! $code_lang is your favourite language!' );
}*/

/*void main( )
{
var a = 5; // normal variable declaration
final b = 6; // variable declaration using final keyword
print( 'Value of a is : $a and Value of b is $b' );
a = 6; // changing the value of normal variable
//b = 7; // changing the value of variable with final keyword
print( 'Value of a is : $a and Value of b is $b' );
}*/

/*import 'package:dart_application_1/dart_application_1.dart' as dart_application_1;

void main(List<String> arguments) {
  print('Hello world: ${dart_application_1.calculate()}!');
}*/

//This is the first Dart program
/*void main() {
  var a = 5, b = 6;
  var c = a + b;
  print('Sum of two numbers is $c.');
}*/





